﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ShoppingWeb.ApiContainer.Infrastructure;
using ShoppingWeb.ApiContainer.Interfaces;
using ShoppingWeb.Models;
using ShoppingWeb.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWeb.ApiContainer
{
    public class AuthApi : BaseHttpClientWithFactory, IAuthApi
    {
        private readonly IApiSettings _settings;

        public AuthApi(IHttpClientFactory factory, IApiSettings settings)
            : base(factory)
        {
            _settings = settings;
        }
        public async Task<IActionResult> login(User user)
        {
            var message = new HttpRequestBuilder(_settings.BaseAddress)
                                .SetPath(_settings.AuthPath)
                                .HttpMethod(HttpMethod.Post)
                                .GetHttpMessage();

            var json = JsonConvert.SerializeObject(user);
            message.Content = new StringContent(json, Encoding.UTF8, "application/json");

            return await SendRequest<IActionResult>(message);
        }

        public async Task<IActionResult> register(User user)
        {
            var message = new HttpRequestBuilder(_settings.BaseAddress)
                                .SetPath(_settings.AuthPath)
                                .HttpMethod(HttpMethod.Post)
                                .GetHttpMessage();

            var json = JsonConvert.SerializeObject(user);
            message.Content = new StringContent(json, Encoding.UTF8, "application/json");

            return await SendRequest<IActionResult>(message);
        }
    }
}
