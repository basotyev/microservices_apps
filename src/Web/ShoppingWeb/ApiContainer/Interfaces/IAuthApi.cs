﻿using Microsoft.AspNetCore.Mvc;
using ShoppingWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingWeb.ApiContainer.Interfaces
{
    public interface IAuthApi
    {
        Task<IActionResult> register(User user);
        Task<IActionResult> login(User user);
    }
}
